<?php
    $password_data = "password";
    $name_data = "admin";
    $error_log = false;
    if(isset($_POST['przycisk_log'])){
            $username = (string)htmlspecialchars($_POST['username']);
            $password = (string)htmlspecialchars($_POST['password']);
            if( $name_data === $username  and $password_data === $password ){
                session_start();
                $_SESSION['zal'] = "www";
                header('Location: panel_adm.php');
            }else{
                $error_log = true;
            }
    }
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8"/>
        <meta name="author" content="Arkadiusz Więcław">
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>
        <?php if (!isset($_SESSION["zal"])){ ?>
        <div class="okno_logowania">
            <form action="logowanie.php" method="POST">
                    <label for="username">Nazwa użytkownika:</label></br>
                    <input type="text" name="username" required/></br>
                    <label for="password">Hasło:</label></br>
                    <input type="password" name="password" required/></br>
                    <input type="submit" value="Zaloguj się" name="przycisk_log" class="przycisk_log"/>
                    <?php
                        if($error_log === true){
                            ?>
                            <script>alert("Nie prawidlowy login lub haslo") </script>
                           <?php
                        }
                    ?>
            </form>
        </div>
        <?php 
            }else{
                header('Location: panel_adm.php');
            }
        ?>
    </body>
</html>
