<?php
   
   
        function generowanie_kodu(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);    
        $length = rand(8,12);
        $kod = '';
        for ($i = 0; $i < $length; $i++) {
            $kod .= $characters[rand(0, $charactersLength - 1)];
        }
        return $kod;
    }

    if(isset($_GET['generuj_kod'])){
        if (!file_exists('kod.txt')) {
            touch('kod.txt', strtotime('-1 days'));
        }
        $kod = generowanie_kodu();
        $myfile = fopen("kod.txt","w") or die("Unable to open file!");
        fwrite($myfile, $kod);
        fclose($myfile);
        header("Location: /wikarek5/panel_adm.php");
    }

    if(isset($_POST['szablon'])){
        $roz = (string)htmlspecialchars($_POST['roz_file']);
        $size = htmlspecialchars($_POST['size_file']);
        $jed = (string)htmlspecialchars($_POST['jednostka']  . PHP_EOL);
        if(empty($roz)){
            echo "nie wprowadziles rozszerzenia ";
            echo "</br>";  
            $roz = "txt";
        }
        if(empty($size) or !is_numeric($size)){
            echo "wprowadziles rozmiar pusty lub niewlasciwa wartosc  dle tego pola przypisano domyslna wartosc ";
            $size = 1000;
        }

        if(!empty($roz) and !empty($size)){
            $roz .= PHP_EOL;
            $size .= PHP_EOL;
            if (!file_exists('ust_szablonu.txt')) {
                touch('ust_szablonu.txt', strtotime('-1 days'));
            }
            $myfile = fopen("ust_szablonu.txt","w") or die("Unable to open file!");
            fwrite($myfile, $roz);
            fwrite($myfile, $size);
            fwrite($myfile, $jed);
            fclose($myfile);
            header("Location: /wikarek5/panel_adm.php");
        }
        
    }

    if(isset($_GET['logout'])){
        session_destroy();
        session_unset(); 
        header('Location: /wikarek5/logowanie.php');
    }

    function odczyt_wygenerowanego_kodu(){
        $kod = '';
        if (!file_exists('kod.txt')) {
            touch('kod.txt', strtotime('-1 days'));
        }
        $myfile = fopen("kod.txt","r") or die("Unable to open file!");
        $kod = fread($myfile, filesize("kod.txt"));
        fclose($myfile);
        if(empty($kod)){
            $kod = generowanie_kodu();
            $myfile = fopen("kod.txt","w") or die("Unable to open file!");
            fwrite($myfile, $kod);
            fclose($myfile);
        }
        return $kod;
    }

    function odczyt_szablonu(){
        $szablon = [];
        if (!file_exists('ust_szablonu.txt')) {
            touch('ust_szablonu.txt', strtotime('-1 days'));
            $myfile = fopen("ust_szablonu.txt","w") or die("Unable to open file!");
            fwrite($myfile, "txt");
            fwrite($myfile, "1000");
            fwrite($myfile, "B");
            fclose($myfile);
        }
        $myfile = fopen("ust_szablonu.txt","r") or die("Unable to open file!");
        $i =0;
        while(!feof($myfile)){
            $szablon[$i] = fgets($myfile);
            $i++;
        }
        fclose($myfile);
        return $szablon;
    }

    if(isset($_POST['file_send'])){
        $status = check_data();
        if ($status === false){
            echo " Plik nie spelnia wymagan";
        }else{
            $location = (string)htmlspecialchars($_POST['location']);
            $conf = include 'configuration.php';
            $connection = @ftp_connect($conf['host'], $conf['port']);
            if (empty($connection) || @ftp_login($connection, $conf['user'], $conf['password']) === false) {
                die('Nie udało się połączyć z serwerem,popraw zawarosc pliku sluzacego do logowania');
            }
            if (isset($_POST['file_user']) && isset($_POST['file_send'])) {
                ftp_put($connection, $_FILES['file_user']['name'], $_FILES['file_user']['tmp_name']);
            }
        }
    }

    function  konwert_jed($max_rozmiar, $jed){
        if( $jed === 'B'.PHP_EOL){
            return $max_rozmiar;
        }
        elseif($jed === 'KB'.PHP_EOL){
            return $max_rozmiar * 1000;
        }
        elseif($jed === 'MB'.PHP_EOL){
            return ($max_rozmiar * 1000 * 1000);
        }
        else{
            return ($max_rozmiar * 1000 * 1000 * 1000);
        }
    }
    
    function check_file(){
        $szablon = odczyt_szablonu();
        $jed = $szablon[2];
        $max_rozmiar = $szablon[1];
        $max_rozmiar = konwert_jed($max_rozmiar, $jed);
        $ext = $szablon[0];
        $test = explode('.',$_FILES['file_user']['name']);
        $file_ext = strtolower(end($test));
        if (is_uploaded_file($_FILES['file_user']['tmp_name'])) {
            if ($_FILES['file_user']['size'] > $max_rozmiar) {
                echo 'Błąd! Plik jest za duży!';
                return false;
            }
            elseif($file_ext.PHP_EOL !== $ext){
                echo 'Błąd! Plik ma nie wlasciwego rozszerzenie !';
                return false;
            }
            else{
                echo 'Odebrano plik. Początkowa nazwa: '.$_FILES['file_user']['name'];
                return true;
            }
        } else {
           echo 'Błąd przy przesyłaniu danych!';
           return false;
        }    
    }
    
    function check_data(){
        $kod_wys = (string)htmlspecialchars($_POST['kod_wys']);
        $kod = (string)htmlspecialchars(odczyt_wygenerowanego_kodu());
        if ($kod_wys !== $kod){
            echo "nie udalo sie przeslac pliku poniewaz podano zly kod";
            return false;
        }else{
            $status = check_file();
            return $status;
        }
    }
?>

<!DOCTYPE html>
<html lag="pl">
    <head>
        <title>Więcław Arkadiusz </title>
        <link rel="stylesheet" href="style_pal.css">
        <meta charset="UTF-8">
        <meta name="author" content="Arkadiusz">
    </head>
    <body>
    <?php
      session_start();
      if (isset($_SESSION["zal"])){
       
    ?>
            <div id="glowny">
                <div id="panel">
                    <a href="/wikarek5/panel_adm.php/?generuj_kod">Generuj kod</a>
                    <a href="/wikarek5/panel_adm.php/?logout" class="wyl">Wyloguj</a>
                </div>
                <div id="content_form">
                    <form method="POST" action="panel_adm.php">
                    <table>
                        <tr>
                            <td><label for="roz_file">Rozszerzenie pliku: </label></td>
                            <td><input type="text"  name="roz_file" id="roz_file" /></td> 
                        </tr>
                        <tr>
                            <td><label for="size">Rozmiar pliku: </label></td>
                            <td><input type="text"  name="size_file" id="size_file"/></td>
                        </tr>
                        <tr>
                            <td><label for="jednostka">Wybierz jednostke: </label></td>
                            <td><select name="jednostka" id="jednostka">
                                <option value="B">B</option>
                                <option value="KB">KB</option>
                                <option value="MB">MB</option>
                                <option value="GB">GB</option>
                            </select></td>
                        </tr>
                       <tr>
                            <td><input type="submit" value="Zmien szablon" name="szablon" /></td>
                       </tr>
                    </table>
                    </form>

                    <form method="POST" action="panel_adm.php" enctype="multipart/form-data" >
                    <table>
                        <tr>
                            <td><label for="kod_p"> Wygenerowany kod: </label></td>
                            <td><input type="text"  name="kod_wys" id="kod_wys"/></td>
                        </tr>
                        <tr>
                            <td><label for="location">Gdzie umiescic plik: </label></td>
                            <td><input type="text"  name="location" id="location" /></td>
                        </tr>
                        <tr>
                            <td><label for="location">Wybierz plik: </label></td></td>
                            <td><input type="file"  name="file_user" id="file_user"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Wyslij na serwer" name="file_send"/></td>
                        </tr>  
                    </table>
                    </form>
                </div>
                <div id="content">
                    <p>zawartosc  szablonu</p>
                    <?php
                       $tab = [];
                       $tab = odczyt_szablonu(); 
                    ?>
                       <p class="szablon">Rozszerzenie pliku: <?echo $tab[0];?></p>
                       <p class="szablon">Rozmiar pliku: <?echo $tab[1];?></p>
                       <p class="szablon">Jednostka: <?echo $tab[2];?></p>
                    <?php
                       $kod = odczyt_wygenerowanego_kodu();
                    ?>
                        <p class="kod">Wygenerowany kod:  <?echo "$kod";?></p>
                </div>  
            </div>
            <?php  }else{
               header("Location: logowanie.php");
            } 
            
            ?>
    </body>
</html>